#include "fifo.h"

#define QUEUE1_SIZE 20

ring_buffer_t queue_1;

char queue1_buffer[QUEUE1_SIZE];

int ret_status;
int len;

int main(void)
{
	fifo_init(&queue_1, queue1_buffer, QUEUE1_SIZE);
	
        char input_data = 'M';
	ret_status = fifo_put(&queue_1, input_data);
	if (ret_status == 0) 
		printf("\nEnqueue of value %c - PASSED", input_data);
	else
		printf("\nEnqueue of value %c - FAILED", input_data);

	char out_data;
	ret_status = fifo_get(&queue_1, &out_data);
	if (ret_status == 0)
		printf("\nDequeue of value %c - PASSED", out_data);
	else
		printf("\nDequeue of value %c - FAILED", out_data);
	
	if (input_data == out_data) 
		printf("\nEnqueue and dequeue item match- PASSED");
	else
		printf("\nEnqueue and dequeue iten match- FAILED");
		
	char in_data[20] = "fifo test queue";
	for (int i = 0; i < strlen(in_data); i++) {
		ret_status = fifo_put(&queue_1, in_data[i]);
	}
	
	if (ret_status == 0)
		printf("\nEnqueue array- PASSED");
	else
		printf("\nEnqueue array- FAILED");
		
	
	ret_status = fifo_peek(&queue_1, &out_data, 0);
	if (ret_status == 0)
		printf("\nvalue peek%c - PASSED", out_data);
	else
		printf("\nvalue peek %c - FAILED", out_data);
	
		
	ret_status = fifo_is_contain(&queue_1, "fifo");
	if (ret_status == 0)
		printf("\nis contain API- PASSED");
	else
		printf("\nis contain API - FAILED");
	
	ret_status = fifo_is_contain(&queue_1, "fifo");
	if (ret_status == 0)
		printf("\nis contain API- PASSED");
	else
		printf("\nis contain API - FAILED");
		
	ret_status = fifo_is_contain(&queue_1, "queue");
	if (ret_status == 0)
		printf("\nis contain API- PASSED");
	else
		printf("\nis contain API - FAILED");
		
	if (fifo_is_data_avail(&queue_1) == 15)
		printf("\nis data avail- PASSED");
	else
		printf("\nis data avail- FAILED");
		
	printf("\n");
}
