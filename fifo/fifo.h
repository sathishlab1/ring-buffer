#ifndef _FIFO_H
#define _FIFO_H

#include "stdbool.h"
#include "string.h"
#include "stdint.h"
#include "stdio.h"


typedef struct ring_buffer {
	char *buffer;
	int tail;
	int head;
	int size;
	int count;
} ring_buffer_t;

void fifo_init(ring_buffer_t *fifo , uint8_t *buf, uint16_t size);
void fifo_reset(ring_buffer_t *fifo);
int fifo_put(ring_buffer_t *fifo, char data);
int fifo_get(ring_buffer_t *fifo, char *data);
int fifo_is_data_avail(ring_buffer_t *fifo);
int fifo_peek(ring_buffer_t *fifo, char *data, int index);
int fifo_is_contain(ring_buffer_t *fifo, const char *substr);

#endif
