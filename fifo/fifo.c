#include "fifo.h"


void fifo_init(ring_buffer_t *fifo , uint8_t *buf, uint16_t size)
{
	fifo->size = size;
	fifo->buffer = buf;
	fifo->tail = 0;
	fifo->head = 0;
	fifo->count = 0;
}

void fifo_reset(ring_buffer_t *fifo)
{
	fifo->tail = 0;
	fifo->head = 0;
	fifo->count = 0;
}

int fifo_put(ring_buffer_t *fifo, char data)
{
	if (fifo->size == 0 || fifo->buffer == NULL )
		return -1;

	if (fifo->count == fifo->size) {
		fifo->tail = (fifo->tail + 1) % fifo->size;
		fifo->count--;
	}
	
	fifo->buffer[fifo->head] = data;
	fifo->head = (fifo->head + 1) % fifo->size;
	fifo->count++;
	
	return 0;
}

int fifo_get(ring_buffer_t *fifo, char *data)
{
	if (fifo->count == 0 || fifo->buffer == NULL)
	 	return -1;

  	*data = fifo->buffer[fifo->tail];
  	fifo->tail = (fifo->tail + 1) % fifo->size;
  	fifo->count--;
  	
	return 0;
}


int fifo_is_data_avail(ring_buffer_t *fifo)
{
	return fifo->count;
}

int fifo_peek(ring_buffer_t *fifo, char *data, int index)
{
 	if (fifo->count == 0 || fifo->buffer == NULL)
	 	return -1;
	 	
	if (index >= fifo->count)
		return -1;

	*data = fifo->buffer[(fifo->tail + index) % fifo->size];  
		 
	return 0;
}


int fifo_is_contain(ring_buffer_t *fifo, const char *substr)
{
	const char *pattern = NULL;
	char data = 0;
	int index = 0;
	int temp_index = 0;
	
	while (index < fifo->count) {
		pattern = substr;
		temp_index = index;
		if (fifo_peek(fifo, &data, temp_index) == -1)
			return -1;

		while (temp_index < fifo->count && *pattern && data == *pattern) {
			//printf("\nmy = %c, pat = %c", data, *pattern);
			temp_index++;
			pattern++;
			fifo_peek(fifo, &data, temp_index);
		}
		
		if (!*pattern)
		    	  return 0;
		index++;
	}
	
	return -1;
}
