#ifndef _RING_BUFF_G_H
#define _RING_BUFF_G_H
#include <string.h>
#include <stdio.h>

typedef unsigned int uint16_t;
typedef unsigned char uint8_t;

typedef enum g_typename {
	CHAR = sizeof(char),
	SHORT = sizeof(short),
	INT = sizeof(int),
	LONG = sizeof(long),
	LONG_LONG = sizeof(long long),
	FLOAT = sizeof(float),	
	DOUBLE = sizeof(double)
} g_datatype_et;


typedef enum flowcontrol {
	FLOW_CONTROLLED,
	DISCARD_ON_OVERFLOW,
	PARTIAL_RW
} flow_ctrl_et;

typedef union generic_datatype {
	char ug_char;
	short ug_short;
	int ug_int;
	long ug_long;
	long long ug_long_long;
	float ug_float;
	double ug_double;
} g_datatype_ut;

typedef union generic_dtype {
	char ug_char;
	short ug_short;
	int ug_int;
	long ug_long;
	long long ug_long_long;
	float ug_float;
	double ug_double;
} g_dtype_ut;

typedef struct queue_info {
   	uint8_t head_index;
   	uint8_t tail_index;
   	uint8_t total_items;
	uint8_t size;
	enum g_typename g_type;
	enum flowcontrol flow_ctrl;
   	union generic_datatype *queue_buffer; 
} queue_info_st;

void queue_init_generic(struct queue_info *queue, union generic_datatype *generic, enum g_typename, int queue_len);
int queue_put_generic(struct queue_info *queue, union generic_dtype *generic, int len);
int queue_get_generic(struct queue_info *queue, union generic_dtype *generic, int *len);
void queue_reset(struct queue_info *queue);
int queue_config(struct queue_info *queue, int config);


#endif //_RING_BUFF_H
