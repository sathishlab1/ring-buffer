#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ring_queue_g.h"

#define SUCCESS  0
#define FAILURE -1

static uint8_t is_queue_full(struct queue_info *queue);
static uint8_t is_queue_empty(struct queue_info *queue);
static uint8_t queue_no_of_items(struct queue_info *queue);

void queue_init_generic(struct queue_info *queue, union generic_datatype *generic, enum g_typename g_type, int queue_len)
{
        if (queue != NULL) {
		queue->head_index = 0;
		queue->tail_index = 0;
        	queue->total_items = 0;
		queue->g_type = g_type;
		queue->flow_ctrl = FLOW_CONTROLLED;
		queue->size = queue_len;
        	queue->queue_buffer = generic;
	}
}


int queue_put_generic(struct queue_info *queue, union generic_dtype *generic, int len)
{
	char * buffer_p = (char *)generic;
	int buff_len = 0;	

	if (is_queue_full(queue) || queue->queue_buffer == NULL || queue->size == 0) {
		return FAILURE;
	}

	if ((queue->size - queue_no_of_items(queue)) < len) {
  		switch(queue->flow_ctrl) {
		case FLOW_CONTROLLED:
			return FAILURE;
		break;
		case PARTIAL_RW:
			len = queue->size - queue_no_of_items(queue);
		break;
		} 	
	}
	
	buff_len = len;
        while (buff_len) {
		memcpy(&queue->queue_buffer[queue->head_index], buffer_p, queue->g_type);
		buffer_p += queue->g_type;
		queue->head_index = (queue->head_index + 1) % queue->size;
		queue->total_items++;
		buff_len--;
	}
	return SUCCESS;
}


int queue_get_generic(struct queue_info *queue, union generic_dtype *generic, int *len)
{
	char *buffer_p = (char *)generic;
	int buff_len = 0;

	if (is_queue_empty(queue) || queue->queue_buffer == NULL || queue->size == 0) {
		return FAILURE;
        }

	if (queue_no_of_items(queue) < *len) {
		switch(queue->flow_ctrl) {
		case FLOW_CONTROLLED:
			return FAILURE;
		break;
		case PARTIAL_RW:
			*len = queue_no_of_items(queue);
		break;
		} 	
	}

	buff_len = *len;
	while (buff_len) {
		memcpy(buffer_p, &queue->queue_buffer[queue->tail_index], queue->g_type);
		buffer_p += queue->g_type;
		queue->tail_index = (queue->tail_index + 1) % queue->size;
		queue->total_items--;
		buff_len--;
	}
	return SUCCESS;
}


void queue_reset(struct queue_info *queue)
{
	queue->head_index = 0;
	queue->tail_index = 0;
        queue->total_items = 0;
}

int queue_config(struct queue_info *queue, int config)
{
	switch (config) {
	case FLOW_CONTROLLED:
	case PARTIAL_RW:
		queue->flow_ctrl = (enum flowcontrol)config;
		return SUCCESS;
	break;
	default:
		return FAILURE;
	break;
	} 	
}

static uint8_t is_queue_full(struct queue_info *queue)
{
	return (queue->total_items == queue->size);
}

static uint8_t is_queue_empty(struct queue_info *queue)
{
	return !queue->total_items;
}

static uint8_t queue_no_of_items(struct queue_info *queue)
{
	return queue->total_items;
}
