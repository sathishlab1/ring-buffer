#include "ring_queue.h"

#define QUEUE1_SIZE 5
#define QUEUE2_SIZE 5
#define QUEUE_UNINIT_SIZE 5

queue_info_st queue_1;
queue_info_st queue_2;
queue_info_st queue_uninit;

char queue1_buffer[QUEUE1_SIZE];
char queue2_buffer[QUEUE2_SIZE];
char queue_uninit_buffer[QUEUE_UNINIT_SIZE];


char q1_data[QUEUE1_SIZE] = {"hello"};
char q2_data[QUEUE2_SIZE] = {"test1"};
char q_uninit_data[QUEUE_UNINIT_SIZE] = {"hello"};

int ret_status;
int len;
int main(void)
{
	queue_init(&queue_1, queue1_buffer, QUEUE1_SIZE);
	queue_init(&queue_2, queue2_buffer, QUEUE2_SIZE);

	/* test 1: multiple queues in same application */

        char input_data = 'M';
	ret_status = queue_put(&queue_1, input_data);
	if (ret_status == 0) 
		printf("\nEnqueue of value %c - PASSED", input_data);
	else
		printf("\nEnqueue of value %c - FAILED", input_data);

	char out_data;
	ret_status = queue_get(&queue_1, &out_data);
	if (ret_status == 0)
		printf("\nDequeue of value %c - PASSED", out_data);
	else
		printf("\nDequeue of value %c - FAILED", out_data);
	
	if (input_data == out_data) 
		printf("\nEnqueue and dequeue item match- PASSED");
	else
		printf("\nEnqueue and dequeue iten match- FAILED");
	
	input_data = 'Z';
	ret_status = queue_put(&queue_2, input_data);
	if (ret_status == 0)
		printf("\nEnqueue of value %c - PASSED", input_data);
	else
		printf("\nEnqueue of value %c - FAILED", input_data);

	ret_status = queue_get(&queue_2, &out_data);
	if (ret_status == 0)
		printf("\nDequeue of value %c - PASSED", out_data);
	else
		printf("\nDequeue of value %c - FAILED", out_data);
	
	if (input_data == out_data) 
		printf("\nEnqueue and dequeue item match- PASSED");
	else
		printf("\nEnqueue and dequeue iten match- FAILED");
	
	/* test 2 : queueing more than data size */
	
	len = QUEUE1_SIZE + 1; 
        ret_status = queue_put_bulk(&queue_1, q1_data, len);
	if (ret_status == -1)
		printf("\nEnqueue more than queue size - PASSED");
	else
		printf("\nEnqueue more than queue size - FAILED");
	
	/* test 3 : queueing and Dequeing bulk data */

	len = QUEUE1_SIZE;
	ret_status = queue_put_bulk(&queue_1, q1_data, len);
	if (ret_status == 0)
		printf("\nEnqueue bulk data - PASSED");
	else
		printf("\nEnqueue bulk data - FAILED");

	len = QUEUE1_SIZE;
	ret_status = queue_get_bulk(&queue_1, q2_data, &len);
	if (ret_status == 0)
		printf("\nDequeue bulk data %s - PASSED", q2_data);
	else
		printf("\nDequeue bulk data %s - FAILED", q2_data);
	
	/* test 4: queueing without initialization */

	len = sizeof(q_uninit_data);
	ret_status = queue_put_bulk(&queue_uninit, q_uninit_data, len);      
	if (ret_status == -1)
		printf("\nqueueing without initialization - PASSED");
	else
		printf("\nqueueing without initialization - FAILED");

	ret_status = queue_get_bulk(&queue_uninit, q_uninit_data, &len);      
	if (ret_status == -1)
		printf("\ndequeueing without initialization - PASSED");
	else
		printf("\ndequeueing without initialization - FAILED");

	/* test 5: dequeuing without queueing after reset and flush */

	len = sizeof(queue_1);
	queue_reset(&queue_1);
	ret_status = queue_get_bulk(&queue_1, q1_data,&len);
	if (ret_status == -1)
		printf("\ndequeueing without queueing after reset- PASSED");
	else
		printf("\ndequeueing without queueing after reset- FAILED");

	len = sizeof(queue_1);
	ret_status = queue_flush(&queue_1, q1_data, &len);
	ret_status = queue_get_bulk(&queue_1, q1_data,&len);
	if (ret_status == -1)
		printf("\ndequeueing without queueing after flush- PASSED");
	else
		printf("\ndequeueing without queueing after flush- FAILED");
	printf("\n");
}
