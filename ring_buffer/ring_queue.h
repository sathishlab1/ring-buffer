#ifndef _RING_BUFF_H
#define _RING_BUFF_H
#include <stdio.h>
#include <string.h>

typedef unsigned int uint16_t;
typedef unsigned char uint8_t;

typedef enum flowcontrol {
	FLOW_CONTROLLED,
	DISCARD_ON_OVERFLOW,
	PARTIAL_RW
} flow_ctrl_et;

typedef struct queue_info {
   	uint8_t head_index;
   	uint8_t tail_index;
   	uint8_t total_items;
	uint8_t size;
	enum flowcontrol flow_ctrl;
   	char *queue_buffer; 
} queue_info_st;

void queue_init(struct queue_info *queue, char *queue_buf, int queue_len);
int queue_put(struct queue_info *queue, char value);
int queue_get(struct queue_info *queue, char *value);
int queue_put_bulk(struct queue_info *queue, char *values, int len);
int queue_get_bulk(struct queue_info *queue, char *values, int *len);
void queue_reset(struct queue_info *queue);
int queue_flush(struct queue_info *queue, char *values, int *len);
int queue_config(struct queue_info *queue, int config); 


#endif //_RING_BUFF_H
