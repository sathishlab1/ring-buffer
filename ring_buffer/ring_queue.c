#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ring_queue.h"

#define SUCCESS  0
#define FAILURE -1

static uint8_t is_queue_full(struct queue_info *queue);
static uint8_t is_queue_empty(struct queue_info *queue);
static uint8_t queue_no_of_items(struct queue_info *queue);

void queue_init(struct queue_info *queue, char *queue_buf, int queue_len)
{
        if (queue != NULL) {
		queue->head_index = 0;
		queue->tail_index = 0;
        	queue->total_items = 0;
		queue->size = queue_len;
		queue->flow_ctrl = FLOW_CONTROLLED;
        	queue->queue_buffer = queue_buf;
	}
}

int queue_put(struct queue_info *queue, char value)
{       
        if (is_queue_full(queue) || queue->queue_buffer == NULL || queue->size == 0) {
		return FAILURE;
	}
	queue->queue_buffer[queue->head_index] = value;
        queue->head_index = (queue->head_index + 1) % queue->size;
	queue->total_items++;
        return SUCCESS;
}

int queue_get(struct queue_info *queue, char *value)
{
        if (is_queue_empty(queue) || queue->queue_buffer == NULL || queue->size == 0) {
		return FAILURE;
        }
	*value = queue->queue_buffer[queue->tail_index];
	queue->tail_index = (queue->tail_index + 1) % queue->size;
	queue->total_items--;
        return SUCCESS;
}

int queue_put_bulk(struct queue_info *queue, char *values, int len)
{
	uint8_t index = 0;
        int ret_result = FAILURE;

	if ((queue->size - queue_no_of_items(queue)) < len) {
  		switch(queue->flow_ctrl) {
		case FLOW_CONTROLLED:
			return FAILURE;
		break;
		case PARTIAL_RW:
			len = queue->size - queue_no_of_items(queue);
		break;
		} 	
	}
        
  	do {
      		ret_result = queue_put(queue, values[index]);
		index++;
  	} while(index < len && ret_result == SUCCESS);
	return ret_result;
}

int queue_get_bulk(struct queue_info *queue, char *values, int *len)
{
  	uint8_t index = 0;
	int ret_result = FAILURE;
       
        if (queue_no_of_items(queue) < *len) {
		switch(queue->flow_ctrl) {
		case FLOW_CONTROLLED:
			return FAILURE;
		break;
		case PARTIAL_RW:
			*len = queue_no_of_items(queue);
		break;
		} 	
	}

	do {
		ret_result = queue_get(queue, &values[index]);
		index++;
        } while(index < *len && ret_result == SUCCESS);
	return ret_result;
}

void queue_reset(struct queue_info *queue)
{
	queue->head_index = 0;
	queue->tail_index = 0;
        queue->total_items = 0;
}

int queue_flush(struct queue_info *queue, char *values, int *len)
{
	int ret_result = FAILURE;
        *len  = queue_no_of_items(queue);
	ret_result = queue_get_bulk(queue, values, len);
	queue_reset(queue);
	return ret_result;;
}

int queue_config(struct queue_info *queue, int config)
{
	switch (config) {
	case FLOW_CONTROLLED:
	case PARTIAL_RW:
		queue->flow_ctrl = (enum flowcontrol)config;
		return SUCCESS;
	break;
	default:
		return FAILURE;
	break;
	} 	
}

static uint8_t is_queue_full(struct queue_info *queue)
{
	return (queue->total_items == queue->size);
}

static uint8_t is_queue_empty(struct queue_info *queue)
{
	return !queue->total_items;
}

static uint8_t queue_no_of_items(struct queue_info *queue)
{
	return queue->total_items;
}
