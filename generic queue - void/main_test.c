#include "ring_queue_gv.h"

#define QUEUE1_SIZE 5
#define QUEUE2_SIZE 5
#define QUEUE3_SIZE 5
#define QUEUE_UNINIT_SIZE 5

queue_info_st queue_1;
queue_info_st queue_2;
queue_info_st queue_3;
queue_info_st queue_uninit;

char queue1_buffer[QUEUE1_SIZE];
float queue2_buffer[QUEUE2_SIZE];
int queue3_buffer[QUEUE3_SIZE];
int queue_uninit_buffer[QUEUE_UNINIT_SIZE];


char q1_data[QUEUE1_SIZE] = {"hello"};
float q2_data[QUEUE2_SIZE] = { 2.4, 8.9, 4.7, 23.89, 87.08};
int q3_data[QUEUE3_SIZE] = { 2, 8, 4, 23, 87};
int q_uninit_data[QUEUE_UNINIT_SIZE];

int ret_status;
int len;
int main(void)
{
	queue_init_g(&queue_1, queue1_buffer, CHAR, QUEUE1_SIZE);
	queue_init_g(&queue_2, queue2_buffer, FLOAT, QUEUE2_SIZE);
	queue_init_g(&queue_3, queue3_buffer, INT, QUEUE3_SIZE);
	/* test 1: multiple queues in same application */

	/* queueing and Dequeing char data */

	len = QUEUE1_SIZE;
	ret_status = queue_put_g(&queue_1, q1_data, len);
	if (ret_status == 0)
		printf("\nEnqueue bulk data - PASSED");
	else
		printf("\nEnqueue bulk data - FAILED");

	len = QUEUE1_SIZE;
	ret_status = queue_get_g(&queue_1, q1_data, &len);
	if (ret_status == 0)
		printf("\nDequeue bulk data %s - PASSED", q1_data);
	else
		printf("\nDequeue bulk data %s - FAILED", q1_data);


	/* queueing and Dequeing float data */
       	len = QUEUE2_SIZE;
	ret_status = queue_put_g(&queue_2, q2_data, len);
	if (ret_status == 0)
		printf("\nEnqueue bulk data - PASSED");
	else
		printf("\nEnqueue bulk data - FAILED");

	len = QUEUE2_SIZE;
	ret_status = queue_get_g(&queue_2, q2_data, &len);
	if (ret_status == 0)
		printf("\nDequeue bulk data %f - PASSED", q2_data[3]);
	else
		printf("\nDequeue bulk data %f - FAILED", q2_data[3]);
	

	/* queueing and Dequeing int data */
       	len = QUEUE3_SIZE;
	ret_status = queue_put_g(&queue_3, q3_data, len);
	if (ret_status == 0)
		printf("\nEnqueue bulk data - PASSED");
	else
		printf("\nEnqueue bulk data - FAILED");

	len = QUEUE3_SIZE;
	ret_status = queue_get_g(&queue_3, q3_data, &len);
	if (ret_status == 0)
		printf("\nDequeue bulk data %d - PASSED", q3_data[4]);
	else
		printf("\nDequeue bulk data %d - FAILED", q3_data[4]);


	/* test 2 : queueing more than data size */
	
	len = QUEUE1_SIZE + 1; 
        ret_status = queue_put_g(&queue_1, q1_data, len);
	if (ret_status == -1)
		printf("\nEnqueue more than queue size - PASSED");
	else
		printf("\nEnqueue more than queue size - FAILED");
	

	/* test 3: queueing without initialization */

	len = sizeof(q_uninit_data);
	ret_status = queue_put_g(&queue_uninit, q_uninit_data, len);      
	if (ret_status == -1)
		printf("\nqueueing without initialization - PASSED");
	else
		printf("\nqueueing without initialization - FAILED");

	ret_status = queue_get_g(&queue_uninit, q_uninit_data, &len);      
	if (ret_status == -1)
		printf("\ndequeueing without initialization - PASSED");
	else
		printf("\ndequeueing without initialization - FAILED");

	/* test 4: dequeuing without queueing after reset*/

	len = sizeof(queue_1);
	queue_reset(&queue_1);
	ret_status = queue_get_g(&queue_1, q1_data,&len);
	if (ret_status == -1)
		printf("\ndequeueing without queueing after reset- PASSED");
	else
		printf("\ndequeueing without queueing after reset- FAILED");

	printf("\n");
}
